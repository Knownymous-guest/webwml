#use wml::debian::template title="Debian op de Desktop"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<h2>Het Universele Besturingssysteem als uw Desktop</h2>

<p>
  Het Debian Desktop subproject bestaat uit een groep vrijwilligers die het best mogelijke besturingssysteem
  willen maken voor de huis- en werkcomputer. Ons motto is <q>Software die gewoonweg werkt</q>.
  In het kort is het ons doel om Debian, GNU en Linux tot bij het brede publiek te brengen.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>
<h3>Onze basisprincipes</h3>
<ul>
  <li>
    We erkennen dat er vele <a href="https://wiki.debian.org/DesktopEnvironment">grafische werkomgevingen</a> bestaan. We zullen het gebruik
    ervan ondersteunen en ervoor zorgen dat ze goed werken op Debian.
  </li>
  <li>
    We kennen enkel twee belangrijke groepen gebruikers: de beginneling en de expert. We zullen alles doen wat in onze mogelijkheden ligt om voor een
beginneling de zaken heel eenvoudig te maken, en tegelijk willen we het de expert mogelijk maken alles volgens hun wensen af te stellen.
  </li>
  <li>
    We zullen proberen te verzekeren dat software geconfigureerd is voor het meest gewone desktopgebruik. Bijvoorbeeld moet het gewone gebruikersaccount dat standaard aangemaakt wordt tijdens de installatie de rechten hebben om audio en video te spelen, te printen en het systeem te beheren via sudo.
  </li>
  <li>
    <p>
    We zullen proberen ervoor te zorgen dat de vragen (die tot een
    minimum beperkt zouden moeten worden) die aan de gebruiker gesteld
    worden, zinvol zijn, zelfs voor iemand met slechts een minimale
    computerkennis.
    Sommige pakketten in Debian presenteren de gebruiker momenteel moeilijke
    technische details. Het installatiesysteem van Debian zou moeten
    vermijden om de gebruiker technische configuratievragen (via het
    debconf-systeem) te stellen. Voor een beginneling zijn deze vaak
    verwarrend en beangstigend. Voor een expert kunnen ze vervelend en
    overbodig zijn. Mogelijk weet een beginneling niet eens waarover deze
    vragen gaan. En een expert kan zijn grafische werkomgeving configureren
    zoals hij dat verkiest nadat de installatie afgerond is. In ieder geval
    zouden dergelijke Debconf-vragen een lagere prioriteit moeten krijgen.
    </p>
  <li>
    En daaraan werken zal leuk worden.
  </li>
</ul>
<h3>Hoe u kunt helpen</h3>
<p>
  De belangrijkste onderdelen van een Debian subproject zijn niet de mailinglijsten, de webpagina's of de archiefruimte voor de pakketten. Het belangrijkste zijn <em>gemotiveerde personen</em> die dingen doen gebeuren. U hoeft geen officiële ontwikkelaar te zijn om pakketten en patches te maken. Het kernteam van het Debian Desktop project zal ervoor zorgen dat uw werk wordt geïntegreerd. Hier volgen enkele dingen die u kunt doen:
</p>
<ul>
  <li>
    Test de <em>taak</em> <q>Standaard Desktopomgeving</q> (of de taak kde-desktop), door één van onze <a
href="$(DEVEL)/debian-installer/">testimages van de volgende release</a> te installeren en feedback te sturen
naar de <a href="https://lists.debian.org/debian-desktop/">mailinglijst debian-desktop</a>.
  </li>
  <li>
    Werk mee aan het <a href="$(DEVEL)/debian-installer/">installatiesysteem van debian</a>.
    De GTK+ frontend heeft u nodig.
  </li>
  <li>
    Help het <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME team</a>,
    het <a href="https://qt-kde-team.pages.debian.net/">Debian Qt and KDE team</a> of de
    <a href="https://salsa.debian.org/xfce-team/">Debian Xfce groep</a>.
    U kunt helpen met pakketten maken, bugs beoordelen, documentatie, tests, enz.
  </li>
  <li>
    Breng gebruikers bij hoe de huidige Debian desktop taken (desktop, gnome-desktop en kde-desktop) geïnstalleerd en gebruikt kunnen worden.
  </li>
  <li>
    Werk aan het verlagen van de prioriteit van, of het verwijderen van <a href="https://packages.debian.org/debconf">debconf</a>-prompts van pakketten en zorg ervoor dat deze welke noodzakelijk zijn, gemakkelijk te verstaan zijn.
  </li>
  <li>
    Help bij de <a href="https://wiki.debian.org/DebianDesktop/Artwork">grafische vormgeving van de Debian Desktop</a>.
  </li>
</ul>
<h3>Wiki</h3>
<p>
   We hebben een aantal artikels in onze Wiki die u hier kunt vinden:
   <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Sommige Debian Desktop wiki-artikels
   zijn verouderd.
</p>
<h3>Mailinglijst</h3>
<p>
   Dit subproject wordt besproken op de mailinglijst <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
</p>
<h3>IRC Kanaal</h3>
<p>
  We moedigen iedereen (al dan niet ontwikkelaar van Debian) die geïnteresseerd is in Debian Desktop, aan om mee
  te doen op <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).
</p>
<h3>Wie is er betrokken?</h3>
<p>
  Iedereen die wenst, is welkom. Eigenlijk is iedereen van de pkg-gnome, pkg-kde en pkg-xfce groepen indirect betrokken.
  De intekenaars op de mailinglijst debian-desktop zijn actieve medewerkers. Ook de groepen rond het installatiesysteem van debian (debian-installer) en tasksel zijn van belang voor onze doelstellingen.
</p>

<p>
  Deze webpagina wordt onderhouden door <a href="https://people.debian.org/~stratus/">Gustavo Franco</a>.
  Eerdere onderhouders waren
  <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> en
  <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>
