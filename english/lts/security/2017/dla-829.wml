<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two memory management issues were found in the asfdemux element of the
GStreamer <q>ugly</q> plugin collection, which can be triggered via a
maliciously crafted file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.19-2+deb7u1.</p>

<p>We recommend that you upgrade your gst-plugins-ugly0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-829.data"
# $Id: $
