<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In connman, stack-based buffer overflow in <q>dnsproxy.c</q> allows remote attackers
to cause a denial of service (crash) or execute arbitrary code via a crafted
response query string passed to the <q>name</q> variable.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0-1.1+wheezy2.</p>

<p>We recommend that you upgrade your connman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1078.data"
# $Id: $
