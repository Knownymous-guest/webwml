<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000381">CVE-2017-1000381</a>

      <p>The c-ares function ares_parse_naptr_reply(), which is used for
      parsing NAPTR responses, could be triggered to read memory
      outside of the given input buffer if the passed in DNS response
      packet was crafted in a particular way.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.1-3+deb7u2.</p>

<p>We recommend that you upgrade your c-ares packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-998.data"
# $Id: $
