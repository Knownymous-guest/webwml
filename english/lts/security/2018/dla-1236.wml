<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Charles Duffy discovered that the Commandline class in plexus-utils, a
collection of components used by Apache Maven, does not correctly
quote the contents of double-quoted strings. An attacker may use this
flaw to inject arbitrary shell commands.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.5.15-4+deb7u1.</p>

<p>We recommend that you upgrade your plexus-utils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1236.data"
# $Id: $
