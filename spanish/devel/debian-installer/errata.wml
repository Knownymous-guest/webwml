#use wml::debian::template title="Erratas del instalador de Debian"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="6c2cebca608641bef7b9c95b5ebe6fceba3e9d6d" maintainer="Laura Arjona Reina"

<h1>Erratas en «<humanversion />»</h1>

<p>
Esta es una lista de problemas conocidos en la versión «<humanversion />»
del instalador de Debian. Si usted no ve aquí listado su problema,
por favor envíenos un <a href="$(HOME)/releases/stable/amd64/ch05s04.html#submit-bug">informe de instalación</a>
describiéndolo.
</p>

<dl class="gloss">

<dt>GNOME puede fallar al iniciarse en algunas instalaciones de máquina virtual</dt>
  <dd>Durante las pruebas de la imagen de Stretch Alfa 4, se ha visto que GNOME
  puede fallar al iniciarse dependiendo de la instalación usada en máquinas virtuales.
  Parece que usar cirrus como una emulación del chip de vídeo funciona bien.
  <br/>
  <b>Estado:</b> Se está investigando.</dd>

<dt>Las instalaciones con escritorio pueden fallar si se utiliza solo el CD 1</dt>
<dd>Debido a restricciones de espacio en el primer CD, no caben en el CD 1 todos los 
paquetes que se esperan para el escritorio GNOME. Para una instalación exitosa, 
use fuentes de paquete adicionales (p. ej. un segundo CD o una réplica en red)
o use un DVD.
<br /> <b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD 1.</dd>

<dt>Tema usado en el instalador</dt>

     <dd>Todavía no hay arte gráfico para Buster, y el instalador aún usa
     el tema de Stretch.
     <br />
     <b>Estado:</b>
       El tema <a href="https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html">
       futurePrototype</a> está siendo integrado y debería estar disponible para la
       versión posterior a Buster Alfa 5.</dd>
       
<dt>Fallos en los métodos abreviados en el editor nano</dt>

      <dd>La versión de nano usada dentro del instalador de Debian no 
      utiliza los mismos métodos abreviados que el paquete nano estándar 
      (en sistemas instalados). En particular la sugerencia de usar <tt>C-s</tt>
      para <q>Guardar</q> parece ser un problema para las personas que usan
      el instalador en una consola en serie
      (<a href="https://bugs.debian.org/915017">#915017</a>).
      <br />
      <b>Estado:</b> Se está investigando.</dd>
      
<dt>Renderización fallida para guyaratí</dt>

      <dd>Se descubrió demasiado tarde para la publicación D-I Buster 
      Alfa 4 que la renderización para el lenguaje guyaratí estaba dañada,
      debido a un error con los tipos de letra requeridos para este lenguaje
      (<a href="https://bugs.debian.org/911705">#911705</a>).
      <br />
      <b>Estado:</b> Arreglado en Buster Alfa 5.</dd> 
    
 	 <!-- las cosas deberían ir mejor a partir de Jessie Beta 2...

         <dt>Soporte GNU/kFreeBSD</dt>
         <dd>Las imágenes de instalación de GNU/kFreeBSD tienen varios 
         fallos importantes
         (<a href="https://bugs.debian.org/757985">#757985</a>,
         <a href="https://bugs.debian.org/757986">#757986</a>,
         <a href="https://bugs.debian.org/757987">#757987</a>,
        <a href="https://bugs.debian.org/757988">#757988</a>). Los adaptadores
        agradecerían manos que ayuden a volver a tener en forma el instalador!</dd>

-->
<!-- algo obsoleto por el primer "cambio importante" mencionado en el anuncio de 20140813...

	<dt>Accesibilidad del sistema instalado</dt>
	<dd>Aunque se usen tecnologías de accesibilidad durante el proceso de 
	instalación, puede que éstas no se habiliten de manera automática en el sistema
	instalado.
	</dd>
-->

<!-- se deja esto para un posible futuro uso...
	<dt>Las instalaciones de escritorio en i386 no funcionan usando sólo el CD#1</dt>
	<dd>Debido a las restricciones de espacio en el primer CD, no todos los paquetes
	esperados para el escritorio GNOME caben en el CD#1. Para una instalación exitosa,
	use fuentes de paquetes extra (p.ej. un segundo CD o una réplica en la red) o use
	un DVD.
	<br />
	<b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Posibles problemas con el arranque UEFI en amd64</dt>
	<dd>Ha habido algunos informes de problemas para arrancar el instalador de
	Debian en modo UEFI en sistemas amd64. Algunos sistemas aparentemente no arrancan
	de manera fiable usando <code>grub-efi</code>, y algunos otros muestran problemas
	de corrupción de los gráficos al visualizar la pantalla inicial («splash») de la instalación.
	<br />
	Si encuentra alguno de estos problemas, por favor remita un informe de error dando el máximo
	detalle posible, tanto sobre los síntomas, como sobre su hardware - esto debería ayudar al 
	equipo a resolver estos fallos. Para rodear el problema, por ahora, pruebe a desactivar UEFI
	e instalar usando la <q>BIOS antigua («Legacy»)</q> o el <q>modo alternativo («Fallback»)</q>.
	<br />
	<b>Estado:</b> Pueden aparecer más correcciones de fallos en las distintas versiones de Wheezy.
	</dd>
-->

<!-- ditto...
	<dt>i386: se necesita más de 32mb de memoria para instalar</dt>
	<dd>
	La cantidad mínima de memoria que se necesita para instalar en i386
	es 48m, en lugar de los 32mb anteriores. Esperamos reducir los requisitos
	de nuevo a 32mb más adelante. Los requisitos de memoria pueden haber cambiado
	también para otras arquitecturas.
	</dd>
-->

</dl>


