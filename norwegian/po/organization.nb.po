msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2014-05-01 16:28+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "nåværende"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "medlem"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "bestyrer"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Koordinator for stabil utgave"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "wizard"

#: ../../english/intro/organization.data:39
#, fuzzy
#| msgid "chairman"
msgid "chair"
msgstr "styreformann"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "sekretær"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "Styremedlemmer"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "Distribusjon"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Publisitet"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr "Støtte og infrastruktur"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "Leder"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "Teknisk komité"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "Sekretær"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "Utviklingsprosjekter"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "FTP-arkiv"

#: ../../english/intro/organization.data:96
msgid "FTP Masters"
msgstr "FTP-ansvarlige"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "FTP-assistenter"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "Individuelle pakker"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "Utgavekoordinering"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr "Utgiverlag"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "Kvalitetskontroll"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "Installasjonsystems-gruppen"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "Utgivelsesnotater"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "CD-avtrykk"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "Produksjon"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:149
msgid "Autobuilding infrastructure"
msgstr "Infrastruktur for automatisk bygging"

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:159
msgid "Buildd administration"
msgstr "Buildd-administrasjon"

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "Dokumentasjon"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr "List over pakker som trenger arbeid og mulige pakker"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "Porteringer"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "Spesielle systemer"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "Bærbare"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "Brannvegger"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "Innebygde system"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "Pressekontakt"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "Nettsider"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr "Debian kvinneprosjekt"

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "Begivenheter"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Teknisk komité"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr "Partnerprogram"

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr "Koordinering av maskinvaredonasjon"

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "Brukerstøtte"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr "Feilrapportsystem"

#: ../../english/intro/organization.data:379
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Postlisteadministrasjon og -arkiv"

#: ../../english/intro/organization.data:387
msgid "New Members Front Desk"
msgstr "Resepsjon for nye medlemmer"

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr "Debian kontoforvaltere"

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Nøkkelring-koordinatore (PGP og GPG)"

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "Sikkerhetsgruppen"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "Konsulentsiden"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "Side med CD-leverandører"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "Retningslinjer"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "Systemadministrasjon"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Dette er adressen du kan bruke når du kommer imot problemer med Debians "
"maskiner, slik som passordproblemer eller behov for å ha en særskilt pakke "
"installert."

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Om du har problemer med Debians maskinvare, se på <a href=\"https://db."
"debian.org/machines.cgi\">Debians maskinside</a>. Denne skulle inneholde "
"adminstrator-opplysninger for hver maskin."

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator av LDAP-utviklerlisten"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "Blåkopier"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "DNS-forvalter"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "Pakkesporingssystem"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Alioth-administratorer"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr "Debian for barn mellom 1 og 99"

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr "Debian for medisinsk praksis og forskning"

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr "Debian for utdanning"

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr "Debian på juridiske kontor"

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr "Debian for folk med funksjonshemning"

#: ../../english/intro/organization.data:486
msgid "Debian for science and related research"
msgstr "Debian for naturvitenskap og relatert forskning"

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian for utdanning"

#~ msgid "Live System Team"
#~ msgstr "Gruppen for «live»-system"

#~ msgid "Auditor"
#~ msgstr "Revisor"

#~ msgid "Publicity"
#~ msgstr "Publisitet"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian utviklernøkkelringlag"

#, fuzzy
#~| msgid "Release Team"
#~ msgid "Volatile Team"
#~ msgstr "Utgiverlag"

#~ msgid "Vendors"
#~ msgstr "Leverandører"

#, fuzzy
#~ msgid "Release Assistants"
#~ msgstr "Utgavekoordinering"

#~ msgid "Mailing List Archives"
#~ msgstr "Postliste-arkiver"

#~ msgid "Mailing list"
#~ msgstr "Postliste"

#~ msgid "Installation"
#~ msgstr "Installasjon"

#~ msgid "Delegates"
#~ msgstr "Delegater"

#, fuzzy
#~ msgid "Installation System for ``stable''"
#~ msgstr "Installasjonsystems-gruppen"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian for ikke-kommersielle organisasjoner"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Det universelle operativsystemet som ditt skrivebord"

#~ msgid "Accountant"
#~ msgstr "Regnskapsfører"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinering av nøkkelsignering"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Navnene på de individuelle buildd-administratorene kan fins også på <a "
#~ "href=\"http://www.buildd.net\">http://www.buildd.net</a>. Velg en "
#~ "arkitektur og en distribusjon for å se tilgjengelig buildd-er og deres "
#~ "administrator."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Administratorene har ansvaret for at buildd for en spesiell arkitektur "
#~ "(arch) kan nåes på <genericemail arch@buildd.debian.org>, for eksempel "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid "Marketing Team"
#~ msgstr "Markedsføringslag"

#~ msgid "Handhelds"
#~ msgstr "Håndholdte"

#~ msgid "APT Team"
#~ msgstr "APT-gruppen"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Utgiverlag for den «stabile» utgaven"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Tilpassede Debian distribusjoner"

#~ msgid "current Debian Project Leader"
#~ msgstr "nåværende leder for Debian-prosjektet"

#~ msgid "Security Audit Project"
#~ msgstr "Sikkerhetsvurderingsprosjekt"

#~ msgid "Testing Security Team"
#~ msgstr "Lag for sikkerhet testing"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-administratorer"
